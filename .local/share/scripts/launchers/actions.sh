#!/usr/bin/env sh

action_screenshot () { bexec flameshot gui -d 1000; }

action_lock () { bexec sexec i3-lock-with-blur.sh; }

action_i3_logout () { i3-msg exit; }
action_bspwm_logout () { bspc quit; }
action_awesomewm_logout () { awesome-client 'awesome.quit()'; }

action_jdm () { sexec bin/jdm $@; }

action_suspend () { systemctl suspend; }
action_lock_and_suspend () { lock && suspend; }

action_hibernate () { systemctl hibernate; }
action_lock_and_hibernate () { lock && hibernate; }

action_reboot () { systemctl reboot; }
action_shutdown () { systemctl poweroff -i; }

action_off_screen () { sleep 1 && xset dpms force off; }
action_lock_and_off_screen () { lock && off_screen; }

action_bashtop () { bashtop; }
action_calendar () { cal -n 9 -S && read -N 1; }
action_fkill () { sexec bin/fkill; }
action_htop () { htop; }
action_japanese () { echo "" | fedit - 0 0 | xsel -b; }
action_clipboard_edit () { xsel -o -b | fedit - 0 0 | xsel -b; }
action_nmtui () { nmtui; }
action_translate () { trans -I; }
action_weather () { curl 'wttr.in/Lviv?n&F' | less > /dev/tty; }

# action_commands='{
#   "screenshot": "sleep 1 && flameshot gui;",
#   "lock": "sexec i3-lock-with-blur.sh;",
#   "i3_logout": "i3-msg exit;",
#   "switch": "sexec launchers/jdm.sh;",
#   "lock_and_switch": "lock && switch;",
#   "suspend": "systemctl suspend;",
#   "lock_and_suspend": "lock && suspend;",
#   "hibernate": "systemctl hibernate;",
#   "lock_and_hibernate": "lock && hibernate;",
#   "reboot": "systemctl reboot;",
#   "shutdown": "sleep 1 && xset dpms force off;",
#   "off_screen": "systemctl poweroff -i;",
#   "lock_and_off_screen": "lock && off_screen;"
# }'

get () {
  actions=(
    "screenshot"
    "bashtop"
    "calendar"
    "fkill"
    "htop"
    "clipboard_edit"
    "japanese"
    "lock"
    "nmtui"
    "translate"
    "weather"
    "i3_logout"
    "bspwm_logout"
    "awesomewm_logout"
    "lock_and_switch"
    "suspend"
    "lock_and_suspend"
    "hibernate"
    "lock_and_hibernate"
    "reboot"
    "shutdown"
    "off_screen"
    "lock_and_off_screen"
  )

  printf "%s\n" "${actions[@]}"
  sexec launchers/jdm.sh get | awk '{ print "jdm "$0 }'
}

run_detached () {
  local action="$@"

  bexec action_$action
}

run () {
  local action="$@"

  action_$action
}

menu () {
  command="$(get | mselect)"
  run "$command"
}

window () {
  wexec $0 menu
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
esac
