#!/usr/bin/env sh

get () {
  prefix=${PASSWORD_STORE_DIR}
  password_files=( "$prefix"/**/*.gpg )
  password_files=( "${password_files[@]#"$prefix"/}" )
  password_files=( "${password_files[@]%.gpg}" )

  printf '%s\n' "${password_files[@]}"
}

run () {
  local password="$1"

  bexec pass show -c "$password"
}

menu () {
  command="$(get | mselect)"
  run "$command"
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
esac
