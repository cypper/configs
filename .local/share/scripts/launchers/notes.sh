#!/usr/bin/env sh

notes_dir=$XDG_DATA_HOME/notes

get () {
  ls $notes_dir
}

run () {
  local choice="$1"
  local user_input="$2"

  mkdir -p "$notes_dir"
  [[ -n "$choice" ]] && fedit "$notes_dir/$choice" last 0
  [[ ! -n "$choice" && -n "$user_input" ]] && fedit "$notes_dir/$user_input" last 0
}

menu () {
  result="$(get | MSELECT_QUERY=true mselect)"

  input=$(echo "$result" | awk 'NR==1')
  choice=$(echo "$result" | awk 'NR==2')

  run "$choice" "$input"
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2" "$3";;
esac
