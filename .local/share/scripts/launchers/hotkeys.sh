#!/usr/bin/env sh

declare -A HOTKEYS

HOTKEYS[A]="actions.sh window"
HOTKEYS[a]="applications.sh window"
HOTKEYS[C]="actions.sh run calendar"
HOTKEYS[c]="configs.sh window"
HOTKEYS[e]="actions.sh run clipboard-edit"
HOTKEYS[f]="actions.sh run fkill"
HOTKEYS[j]="actions.sh run japanese"
HOTKEYS[l]="launcher.sh window"
HOTKEYS[m]="actions.sh run nmtui"
HOTKEYS[n]="notes.sh window"
HOTKEYS[p]="pass.sh window"
HOTKEYS[t]="actions.sh run translate"
HOTKEYS[w]="nodes.sh window"
HOTKEYS[W]="actions.sh run weather"

get () {
  HINTS=""
  for key in "${!HOTKEYS[@]}"; do HINTS="$HINTS$key - ${HOTKEYS[$key]}\n"; done
  printf "$HINTS" | sort
}

run () {
  local script="${HOTKEYS[$1]}"

  [[ ! -z $script ]] && sexec launchers/launchers-scripts.sh run $script
}

menu () {
  get
  read -N 1 HOTKEY
  run $HOTKEY
}

window () {
  wexec $0 menu
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
esac
