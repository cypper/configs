#!/usr/bin/env sh

LAUNCHERS_DIR="$XDG_DATA_HOME/scripts/launchers"

get () {
  ls --color=never $LAUNCHERS_DIR | awk '
    { print $1" window" }
    { print $1" window-detached" }
  '
}

run () {
  $LAUNCHERS_DIR/$@
}

menu () {
  command="$(get | mselect)"
  run "$command"
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2" "$3" "$4" "$5" "$6" "$7";;
esac
