#!/usr/bin/env sh

cd $HOME

get () {
  git --git-dir=$HOME/.configs.git --work-tree=$HOME ls-files
}

run () {
  local file="$1"

  [[ -n "$file" ]] && wexec fedit "$HOME/$file" 1 1
}

menu () {
  command="$(get | mselect)"
  run "$command"
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
esac
