#!/usr/bin/env sh

monitor="$(printf "%s" "$(sexec monitors.sh dimentions)" | awk '{print $1}')"

case "$(sexec get-current-wm.sh)" in
  "bspwm") MONITOR=$monitor polybar --reload mainbar-bspwm;;
esac

