#!/usr/bin/env sh

STATUS_BAR_WIDTH="$(printf "%s" "$(sexec monitors.sh dimentions)" | awk -F '[x/+]' 'NR==1 {print $1}')"
STATUS_BAR_HEIGHT="20"

sexec status-bars/status-bar.sh \
  | lemonbar -n "lemonbar" \
    -F '#f8f8f2' \
    -o -1 \
    -f "Noto Mono" -f "Font Awesome 5 Free:style=Solid:size=15" \
    -g "$(( STATUS_BAR_WIDTH - 7 ))x$STATUS_BAR_HEIGHT+3+3"
    # -g "$(( STATUS_BAR_WIDTH - 7 - 70 ))x$STATUS_BAR_HEIGHT+3+3"
