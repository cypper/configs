#!/usr/bin/env sh

HIDDEN_WINDOWS=""
DELIMITER=")("

while read info; do
  node="$(printf "%s" "$info" | cut -d ' ' -f 1)"
  title="$(printf "%s" "$info" | cut -d ' ' -f 3- | awk '{ print substr($0, 0, 20) }')"

  case "$(sexec get-current-wm.sh)" in
    "bspwm")
      if bspc query -N -n $node.hidden > /dev/null; then
        HIDDEN_WINDOWS="$HIDDEN_WINDOWS$title$DELIMITER"
      fi
    ;;
  esac
done <<< "$(wmctrl -l | cut -d ' ' -f 1,3,5-)"

[[ -n "$HIDDEN_WINDOWS" ]] && printf "%s" "(${HIDDEN_WINDOWS:0: -2})"

