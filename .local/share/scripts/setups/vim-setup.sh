#/bin/sh

VUNDLE="$XDG_DATA_HOME/vim/bundle/Vundle.vim"

echo "setting up vim:"
if ! [ -d $VUNDLE ]; then
  git clone https://github.com/VundleVim/Vundle.vim.git $VUNDLE
  echo "- vundle installed"
else
  echo "- vundle already installed"
fi

vim -c "PluginUpdate" -c "qall"
if [ -f ./\[Vundle\]\ Installer ]; then
  rm ./\[Vundle\]\ Installer
fi
echo "- vulndle plugins updated"
