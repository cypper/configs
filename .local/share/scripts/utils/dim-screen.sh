#!/usr/bin/env bash

min_brightness=1
max_brightness=1

dim_screen () {
  set_brightness $min_brightness;
}

undim_screen () {
  set_brightness $max_brightness;
}

set_brightness() {
  for monitor_name in $(sexec monitors.sh names); do
    xrandr --output $monitor_name --brightness $1;
  done
}

trap 'exit 0' TERM INT
trap "undim_screen; kill %%" EXIT
dim_screen
sleep 2147483647 &
wait
