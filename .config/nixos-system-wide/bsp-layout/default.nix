{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation rec {
  name = "bsp-layout";

  version = "0.0.9";

  src = fetchFromGitHub {
    owner = "phenax";
    repo = "bsp-layout";
    rev = "${version}";
    sha256 = "0fzcrrdl5w8d83rrncl8cpw03rn7nsp0sgfncczxfcfz0rzi7h1g";
  };

  installPhase = "
    export INSTALL_DIR=$out/bsp-layout;
    export MAN_PAGE_DIR=$out/man/man1;

#    mkdir -p $out/bin
#    cp ${src} $out/bin/psstatus

    echo 'Copying files...';
    mkdir -p $out/bin
    mkdir -p $INSTALL_DIR;
    cp -r ${src}/src/* $INSTALL_DIR/;
    sed -e 's/{{VERSION}}/${version}/g' -e \"s|/usr/lib/|$out/|g\" < ${src}/src/layout.sh > $out/bin/bsp-layout; # Replace version number
    chmod +x $INSTALL_DIR/layouts/*.sh;
    chmod +x $INSTALL_DIR/layout.sh;
    chmod +x $out/bin/bsp-layout;

    echo 'Creating manpage...';
    mkdir -p $MAN_PAGE_DIR;
    # cp ${src}/bsp-layout.1 $MAN_PAGE_DIR/bsp-layout.1;
    sed 's/{{VERSION}}/${version}/g' < ${src}/bsp-layout.1 > $MAN_PAGE_DIR/bsp-layout.1; # Replace version number
    chmod 644 $MAN_PAGE_DIR/bsp-layout.1;

    echo 'Installed bsp-layout';
  ";

  meta = {
    homepage = "https://github.com/phenax/bsp-layout";
    description = "Manage layouts in bspwm (tall and wide)";
  };
}
