# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  # dim = pkgs.callPackage ./dim/default.nix {};
  psstatus = pkgs.callPackage ./psstatus/default.nix {};
  bashtop = pkgs.callPackage ./bashtop/default.nix {};
  bsp-layout = pkgs.callPackage ./bsp-layout/default.nix {};
  picom-dual-kawase = pkgs.callPackage ./picom-dual-kawase/default.nix {};
  dxhd = pkgs.callPackage ./dxhd/default.nix {};
  # lbry-desktop = pkgs.callPackage ./lbry-desktop/default.nix {};

  # vieb = pkgs.callPackage ./vieb/default.nix {};

  # replace with upsteam or at least fix dependencies
  # qutebrowser-dbus-notify = pkgs.libsForQt5.callPackage ./qutebrowser/default.nix {};
  # replace with upsteam
  # tor-browser-bundle-bin = pkgs.libsForQt5.callPackage ./tor-browser-bundle-bin/default.nix {};
in
{
  imports =
    [ # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix
    ];

  # garbage collector
  nix.gc.automatic = true;


  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;

  # Legacy boot loader
  boot.loader.grub.device = "/dev/sdb";
  
  # boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelPackages = pkgs.linuxPackages_5_12;
  # boot.kernelParams = [ "nvidia-drm.modeset=1" ];

  boot.kernel.sysctl = {
    "net.ipv4.ip_default_ttl" = 65;
  };

  hardware.opengl.enable = true;
  hardware.opengl.driSupport32Bit = true;

  # NVIDIA
  # hardware.nvidia.modesetting.enable = true;

  networking.hostName = "cypper"; # Define your hostname.
  # networking.wireless.enable = true;

  networking.networkmanager.enable = true;
  networking.networkmanager.packages = [ pkgs.networkmanager_openvpn ];

  networking.networkmanager.wifi.macAddress = "random";
  networking.networkmanager.ethernet.macAddress = "random";

  hardware.bluetooth.enable = true;

  security.polkit.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Kiev";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  # networking.useDHCP = false;
  # networking.interfaces.enp0s3.useDHCP = true;
  # networking.interfaces.wlp1s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "uk_UA.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };


  nixpkgs.config = {
    allowUnfree = true;
    android_sdk.accept_license = true;
  };

  # Configure keymap in X11
  services.xserver.enable = true;

  # services.xserver.videoDrivers = [ "amdgpu" ];
  services.xserver.videoDrivers = [ "nvidia" ];
  # services.xserver.screenSection = ''
  #   Option  "SLI" "on"
  # '';

  # It is set in xinitrc
  services.xserver.layout = "us,ua";
  services.xserver.xkbOptions = "grp:win_space_toggle, caps:escape";

  # services.xserver.displayManager.startx.enable = true;

  # KDE
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  services.xserver.displayManager.defaultSession = "kde-bspwm";
  services.xserver.displayManager.session = [
    {
      name = "kde-bspwm";
      manage = "desktop";
      start = "env KDEWM=${pkgs.bspwm}/bin/bspwm ${pkgs.plasma-workspace}/bin/startplasma-x11";
    }
  ];

  # GNOME
  # services.xserver.displayManager.gdm.enable = true;
  # services.xserver.displayManager.gdm.nvidiaWayland = true;
  # services.xserver.desktopManager.gnome.enable = true;
  # services.xserver.desktopManager.gnome.flashback.customSessions = [
  #   {
  #     wmName = "bspwm";
  #     wmLabel = "bspwm";
  #     wmCommand = "${pkgs.bspwm}/bin/bspwm";
  #   }
  # ];
  # services.udev.packages = with pkgs; [ gnome.gnome-settings-daemon ];

  services.logind.lidSwitch = "hibernate";
  services.logind.extraConfig = "
    HandlePowerKey=ignore
  ";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  # security.rtkit.enable = true;
  # hardware.pulseaudio = {
  #   enable = true;
  #   support32Bit = true;
  #   systemWide = true;
  #   package = pkgs.pulseaudio;
  # };
  services.pipewire = {
    enable = true;
    # Compatibility shims, adjust according to your needs
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    # sessionManagerArguments = [ "-p" "bluez5.msbc-support=true" ];
  };
  hardware.bluetooth.hsphfpd.enable = true;

  virtualisation.docker.enable = true;

  # boot.kernelModules = [ "kvm-amd" "kvm-intel" ];
  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.cypper = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "networkmanager" "audio" "video" "docker" "libvirtd" "adbusers" ]; # Enable ‘sudo’ for the user.
  };

  users.users.harnosoft = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "networkmanager" "audio" "video" ]; # Enable ‘sudo’ for the user.
  };
  
  users.users.impressit = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "networkmanager" "audio" "video" "docker"]; # Enable ‘sudo’ for the user.
  };

  users.users.test = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "networkmanager" "audio" "video" ]; # Enable ‘sudo’ for the user.
  };

  security.polkit.adminIdentities = [
    "unix-user:cypper"
    "unix-user:harnosoft"
    "unix-user:impressit"
    "unix-user:test"
  ];

  fonts.fonts = with pkgs; [
    dejavu_fonts
    liberation_ttf
    noto-fonts
    font-awesome
    font-awesome-ttf
    source-code-pro
  ];

  environment.sessionVariables = {
    ANDROID_JAVA_HOME = "${pkgs.jdk.home}";
    XDG_CONFIG_HOME = "$HOME/.config";
    XDG_DATA_HOME = "$HOME/.local/share";
    XDG_CACHE_HOME = "$HOME/.cache";
    ZDOTDIR = "$XDG_CONFIG_HOME/zsh";
  };

  # List packages installed in system profile
  environment.systemPackages = with pkgs; [
    # languages support
    cargo python3 nodejs-16_x bash zsh go
    # nix
    home-manager
    # graphics
    vulkan-tools
    # passwords/keys
    pass gnupg pinentry-gtk2
    # monitors
    arandr
    # image viewers/editors
    feh chafa flameshot imagemagick scrot sxiv
    # tray
    stalonetray
    # text editors
    kakoune vim
    # for kakoune
    universal-ctags python39Packages.editorconfig
    # window managers
    bspwm bsp-layout qtile wmctrl sway
    # shortcuts
    sxhkd dxhd
    # browsers
    w3m qutebrowser vieb tor-browser-bundle-bin
    (firefox.override { extraNativeMessagingHosts = [ passff-host ]; })
    # for now just to improve qutebrowser startup
    socat
    # terminals
    alacritty kitty
    # file manager
    vifm lf
    # terminal multiplexer
    tmux
    # networking
    tor wget curl htop
    # notification
    dunst
    # lockscreen
    xss-lock i3lock-color
    # source-highlight
    sourceHighlight
    # translator
    translate-shell
    # pdf viewer
    zathura
    # torrents
    transmission-gtk
    # gtk
    gtk3
    arc-theme
    # windows compozitor
    picom-dual-kawase
    # status bar
    polybar
    lemonbar-xft
    # email
    aerc
    # launcher
    fzf dmenu
    #virtualisation
    docker-compose virt-manager

    # android
    android-studio
    jdk

    ###
    # FUN
    ###
    # music
    cmus
    # games
    lutris wine steam winetricks
    # videos
    # lbry-desktop

    ###
    # GNOME
    ###
    gnomeExtensions.appindicator
    gnome.gnome-tweaks
    gnome.dconf-editor

    ###
    # UTILS
    ###
    stow dragon-drop p7zip
    # lxqt.lxqt-policykit
    psstatus bashtop lm_sensors starship
    # build
    gnumake gcc
    # versioning
    git
    # xorg
    xorg.xinit
    xorg.xdpyinfo
    xdo
    xdotool
    xsel
    xkblayout-state
    xtitle
    # completion
    bash-completion
    # search/replace
    ag fd jq
  ];

  programs.zsh.enable = true;

  programs.ssh.askPassword = "";
  programs.light.enable = true;

  # android adb
  programs.adb.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 8080 ];
  networking.firewall.allowedUDPPorts = [ 22 8080 ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}

