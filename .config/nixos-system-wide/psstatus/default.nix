{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = "psstatus";

  version = "0.2";

  unpackPhase = ":";

  src = fetchurl {
    name = "psstatus";
    url = "https://gitlab.com/cypper/configs/-/raw/aff5256aea300060b82ce2e7983b84e12d907443/.local/bin/psstatus";
    sha256 = "1ivv6dhlz5l6za1703gc3i40paz1j5krqr3hwqzmkg2fd336ckx1";
    executable = true;
  };

  installPhase = "
    mkdir -p $out/bin
    cp ${src} $out/bin/psstatus
  ";
}

