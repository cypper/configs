# Color palette
declare-option str black 'rgb:000000'
declare-option str red 'rgb:d81765'
declare-option str green 'rgb:47d01a'
declare-option str yellow 'rgb:f2df01'
declare-option str blue 'rgb:16b1fb'
declare-option str magenta 'rgb:ff2491'
declare-option str cyan 'rgb:0fdcb6'
declare-option str white 'rgb:ebebeb'


declare-option str orange 'rgb:ff9c36'
# declare-option str pink 'rgb:d81765'
declare-option str purple 'rgb:ff24e4'

declare-option str bright_black 'rgb:242424'
declare-option str gray 'rgb:454545'
declare-option str dimmed_blue 'rgb:0C6894'

# Reference
# https://github.com/mawww/kakoune/blob/master/colors/default.kak
# For code
set-face global value "%opt{purple}"
set-face global type "%opt{purple}+i"
set-face global variable "%opt{orange}"
set-face global module "%opt{red}+i"
set-face global function "%opt{green}"
set-face global string "%opt{yellow}"
set-face global keyword "%opt{magenta}+i"
set-face global operator "%opt{cyan}"
set-face global attribute "%opt{orange}+i"
set-face global comment "%opt{dimmed_blue}+i"
set-face global meta "%opt{red}"
set-face global builtin "%opt{magenta}"

# For markup
set-face global title "%opt{red}"
set-face global header "%opt{orange}"
set-face global bold "%opt{magenta}"
set-face global italic "%opt{purple}"
set-face global mono "%opt{green}"
set-face global block "%opt{cyan}"
set-face global link "%opt{green}"
set-face global bullet "%opt{green}"
set-face global list "%opt{white}"

# Builtin faces
set-face global Default "%opt{white},%opt{black}"
set-face global PrimarySelection "%opt{black},%opt{cyan}"
set-face global SecondarySelection "%opt{black},%opt{cyan}"
set-face global PrimaryCursor "%opt{black},%opt{white}"
set-face global SecondaryCursor "%opt{black},%opt{blue}"
set-face global PrimaryCursorEol "%opt{black},%opt{white}"
set-face global SecondaryCursorEol "%opt{black},%opt{blue}"
set-face global LineNumbers "%opt{white}"
set-face global LineNumberCursor "%opt{blue},%opt{bright_black}"
set-face global LineNumbersWrapped "%opt{orange}"
set-face global MenuForeground "%opt{blue}+b"
set-face global MenuBackground "%opt{blue}"
set-face global MenuInfo "%opt{blue}"
set-face global Information "%opt{white}"
set-face global Error "%opt{red},%opt{black}"
set-face global StatusLine "%opt{white}"
set-face global StatusLineMode "%opt{black},%opt{magenta}+b"
set-face global StatusLineInfo "%opt{blue}"
set-face global StatusLineValue "%opt{orange},%opt{black}"
set-face global StatusCursor "%opt{white},%opt{white}"
set-face global Prompt "%opt{green},%opt{black}"
set-face global MatchingChar "%{black},%opt{green}+bF"
set-face global Whitespace "%opt{gray}"
set-face global WrapMarker Whitespace
# set-face global BufferPadding "%opt{gray},%opt{black}"

# Crosshairs
set-face global CrosshairsLine "default,%opt{bright_black}"
set-face global CrosshairsColumn "default,%opt{bright_black}"
