import draculaDraw


draculaDraw.blood(c, {
  'spacing': {
    'vertical': 6,
    'horizontal': 1
  }
})

# Load existing settings made via :set
config.load_autoconfig()
