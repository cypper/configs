#! /bin/sh

#########################
######### SWAP! #########
#########################
# functions to give herbstluftwm some dynamic tiling behaviors.
# originaly by Aaron "ninjaaron" Christianson <ninjaaron@gmail.com>
# rewritten by Tylo Hart (Wretch) <http://github.com/htylo>

# The generate_command commands echo single strings utilizing the logical operators and
#    chains that can be executed with a single call to herbstclient

# This script currently requires two patches, the negation patch and the query patch,
#    both available at <http://github.com/htylo/hlwm_patches>


#########################
######### USAGE #########
#########################
# For general usage, source this scipt, then substitute the output of a generate command as
#    input for herbstclient

### Functions:
## auto
#    Swaps client in and out of "master" or "stack." If there is only one frame,
#    "auto" splits it. Example keybinding in herbstluftwm's autostart:
#        source /path/to/this/script ; hc $(generate_auto)
## stack_spawn
#    focuses stack and runs command. good wrapper for terminal:
#        source /path/to/this/script ; hc $(generate_stack_spawn)
## master_spawn
#    moves clients in master to stack and runs command. possible wrapper for
#    dmenu_run:
#        source /path/to/this/script ; hc $(generate_master_spawn)
## close
#    If client in master frame is closed, it is replaced by a client from the
#    stack. If stack is empty, it's removed. used like hlwm "close" command:
#        hc spawn /path/to/this/script close

# Note: The "close" command does not work as a chain, because a chain exits one close is called.
#            So to use the close command, spawn this script with argument "close"


# For keybindings, source this file and bind the output of the generate_command functions to a key in autostart.

### ~/.config/herbstluftwm/autostart
# source /path/to/this/scipt
# hc keybind $Mod-a $(generate_auto)
# hc keybind $Mod-d $(generate_stack_spawn [command] )
# hc keybind $Mod-f $(generate_master_spawn [command] )
# hc keybind $Mod-r spawn /path/to/this/scipt close

#########################
##### USER SETTINGS #####
#########################

# The split direction of stack and master
split_direction="horizontal"
# The ratio of master to stack on the screen
split_ratio="0.6667" 
# The layout of the stack
stack_layout="vertical" 
# The direction of the stack relative the master
stack_dir=right

# hc command to return if a frame is empty
query_frame_empty="cquery 0 nwindows"
# hc command to return if there's only 1 frame on a tag
query_frame_n="cquery 1 nframe"
# hc command to return if there is only 1 window on a frame
query_window_alone="cquery 1 nwindows"


#########################
###### Logic Helper #####
#########################

source .config/herbstluftwm/herbstlogic.sh

#########################
## PARSE USER SETTINGS ##
#########################

# Assure stack_sir actually coincides with split_direction.
if [ $split_direction == horizontal ] && (( $stack_dir != "right" || $stack_dir != "left" ))
then 
    echo '$split_direction and $stack_dir mismatch. Defaulting to stack_dir=right'
    stack_dir=right
elif [ $split_direction == vertical ] && (( $stack_dir != "up" || $stack_dir != "down" ))
then 
    echo '$split_direction and $stack_dir mismatch. Defaulting to stack_dir=down'
    stack_dir=down
fi

# Get the direction of the master relative the stack
case $stack_dir in
    r*) master_dir=left
        focus_toggle=$(herbstlogic "focus -e l | focus -e r")
        shift_toggle=$(herbstlogic "shift -e l | shift -e r")
        ;;
    l*) master_dir=right 
        focus_toggle=$(herbstlogic "focus -e l | focus -e r")
        shift_toggle=$(herbstlogic "shift -e l | shift -e r")
        ;;
    u*) master_dir=down
        focus_toggle=$(herbstlogic "focus -e u | focus -e d")
        shift_toggle=$(herbstlogic "shift -e u | shift -e d")
        ;;
    d*) master_dir=up
        focus_toggle=$(herbstlogic "focus -e u | focus -e d")
        shift_toggle=$(herbstlogic "shift -e u | shift -e d")
        ;;
esac




############################
## Construction Functions ##
############################

function generate_auto () {

##################################
# Generates the Logic for auto() #
##################################

# This reads easier bottom-to-top!

### IF SPLIT NEEDED ###
only_frame_true=$(herbstlogic "split $split_direction $split_ratio , $shift_toggle , set_layout $stack_layout ")

# ^
# |
# |

### IF SPLIT NOT NEEDED ###
# There are two windows in the master. Get one outta there!
shift_final_master_not_alone=$(herbstlogic "cycle -1 , shift -e $stack_dir , $focus_toggle")
# If window is alone, we're done. Otherwise, get the other window outta there!
shift_final_master_check_alone=$(herbstlogic "$query_window_alone | $shift_final_master_not_alone")
# Shift to master. If in master, shift to stack.
shift_final=$(herbstlogic if "shift -e $master_dir" then "$shift_final_master_check_alone" else "shift -e $stack_dir")

# ^
# |
# |

### Checking if a shift will result in an empty frame.
# If the other frame is empty, just shift the current window over there.
other_frame_empty=$(herbstlogic "$focus_toggle & $query_frame_empty & $focus_toggle & $shift_toggle")
# If the other frame is not empty, proceed...
other_frame_not_empty=$(herbstlogic "$shift_toggle , cycle -1 , $shift_final")
# Check if the other frame is empty, then do something...
window_alone_do=$(herbstlogic "$other_frame_empty | $other_frame_not_empty")
# If the window on the current frame is alone, do somethings, otherwise just shift it over.
frame_not_empty=$(herbstlogic if "$query_window_alone" then "$window_alone_do" else "$shift_final")
# If the current frame has multiple windows, proceed... otherwise it has one window, or no windows...
only_frame_false=$(herbstlogic if "$query_frame_empty" then "$focus_toggle" else "$frame_not_empty")

# ^
# |
# |

### EXECUTE ###
# Attempt to split the frame, if there are two frames, do normal behavior.
do=$(herbstlogic if "$query_frame_n" then "$only_frame_true" else "$only_frame_false")

echo $do
}

function generate_stack_spawn () {

tag_not_empty=$(herbstlogic "focus -e $stack_dir , spawn $@")
tag_empty=$(herbstlogic "spawn $@ , $(generate_auto) , focus_nth 0 , shift -e $master_dir , focus -e $stack_dir")

do=$(herbstlogic if "$query_frame_n" then "$tag_empty" else "$tag_not_empty")

echo $do
}

function generate_master_spawn () {

tag_not_empty=$(herbstlogic "$(generate_auto) , focus -e $master_dir , shift -e $stack_dir , focus -e $master_dir , spawn $@" )

check_tag_empty=$(herbstlogic "$query_frame_n & $query_frame_empty & spawn $@")
do=$(herbstlogic "$check_tag_empty | $tag_not_empty")

echo $do
}

function generate_close () {

check_master_do=$(herbstlogic "focus -e $stack_dir , shift -e $master_dir")
check_stack=$(herbstlogic "$query_frame_empty & remove")
check_master=$(herbstlogic "$query_frame_empty & $check_master_do")
do=$(herbstlogic "focus -e $master_dir , $check_master , focus -e $stack_dir , $check_stack")

echo $do
}

case $1 in
   close)
        shift
        herbstclient close
        close=$(generate_close)
        herbstclient $close
        exit
        ;;
esac
