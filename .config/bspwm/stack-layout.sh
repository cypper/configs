#!/usr/bin/env bash
# a stack layout for bspwm
# bspc subscribe node_{remove,add} | while read _; do ./stack_layout.sh; done

event="$1"
master_size=.66

jget() {
  # thanks camille
  key=$1
  shift
  var=${*#*\"$key\":}
  var=${var%%[,\}]*}
  echo "$var"
}

vdo() {
  echo "$*"
  "$@"
}

get_master_nodes () {
  bspc query -N '@/1' -n .descendant_of.window
}

get_slave_nodes () {
  bspc query -N '@/2' -n .descendant_of.window
}

get_master_last_node () {
  bspc query -N '@/1' -n last.descendant_of.window | head -n 1
}

if [[ "$event" == "node_remove2" ]]; then
  vdo bspc node -s 
fi

# ensure the count of the master child is 1, or make it so
win_count=$(get_master_nodes | wc -l)
echo event: $event
echo win_count: $win_count
echo crap: "$*"
if [ $win_count -ne 1 ]; then
  new_master=$(get_master_last_node)
  [ -z "$new_master" ] && new_master=$(bspc query -N '@/1' -n newest.descendant_of.window | head -n 1)


  echo "new master: $new_master"

  # move everything into 2 that is not our new_master
  for wid in $(get_master_nodes | grep -v "$new_master"); do
    vdo bspc node "$wid" -n '@/2'
  done

  vdo bspc node "$new_master" -n '@/1'
  [[ "$event" == "node_add" ]] && vdo bspc node '@/2' -C forward
elif [[ "$event" == "node_add" ]]; then
  new_master=$(bspc query -N '@/2' -n last.descendant_of.window | head -n 1)

  echo "new master from slaves: $new_master"

  # move everything into 2 that is not our new_master
  old_master=$(get_master_nodes | head -n 1)
  # for wid in $( | grep -v $new_master); do
  #   vdo bspc node "$wid" -n '@/2'
  # done

  vdo bspc node "$new_master" -s "$old_master"
fi

# amend the split type so we are arranged correctly
# on all stacking children
correct_rotation() {
  # sleep 5
  # sleep 0.5
  node=$1
  want=$2
  have=$(jget splitType "$(bspc query -T -n "$node")")
  # the only bashism
  have=${have:1:${#have}-2}

  if [ ! "$have" = "$want" ]; then
    vdo bspc node "$node" -R 270
  fi
}


vdo correct_rotation '@/' vertical
vdo correct_rotation '@/2' horizontal

stack_node=$(bspc query -N '@/2' -n)
for parent in $(bspc query -N '@/2' -n '.descendant_of.!window' | grep -v "$stack_node"); do
  vdo correct_rotation $parent horizontal
done

bspc node '@/2' -B

# mon_width=$(bspc query -T -m | jq .rectangle.width)
mon_width=$(jget width "$(bspc query -T -m)")

want=$(echo $master_size \* $mon_width | bc -l | sed 's/\..*//')
master="$(bspc query -T -n '@/1')"
[[ -n "$master" ]] && have=$(jget width "$master") || have=0
echo "want: $want, have: $have"
bspc node '@/1' --resize right $((want - have)) 0

# for parent in $(bspc query -N '@/2' -n '.descendant_of.!window' | grep -v "$stack_node"); do
# for parent in $(bspc query -N '@/2' -n '.descendant_of' | grep -v "$stack_node"); do
